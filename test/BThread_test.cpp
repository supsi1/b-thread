//
// Created by kalu on 05/10/20.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN  // in only one cpp file

#include "sum.h"
#include <iostream>
#include <boost/test/unit_test.hpp>


BOOST_AUTO_TEST_SUITE(bThreadTest)
    BOOST_AUTO_TEST_CASE(sumTest){
        BOOST_CHECK_EQUAL(sum(3,2),5);
    };
BOOST_AUTO_TEST_SUITE_END()
